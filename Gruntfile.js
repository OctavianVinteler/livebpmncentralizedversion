module.exports = function (grunt) {
   grunt.initConfig({
      browserify: {
         options: {
           browserifyOptions: {
                    debug: true,
                 }
         },
         dist: {
            options: {
              transform: [
                  ["babelify"]
               ]
            },
            files: {
               // if the source file has an extension of es6 then
               // we change the name of the source file accordingly.
               // The result file's extension is always .js
               "./dist/app.js": ["./src/app.js"]
            }
         }
      },
      watch: {
         scripts: {
            files: ["./src/**/*.js"],
            tasks: ["browserify"]
         }
      }
   });

   grunt.loadNpmTasks("grunt-browserify");
   grunt.loadNpmTasks("grunt-contrib-watch");

   grunt.registerTask("default", ["watch"]);
   grunt.registerTask("build", ["browserify"]);
};
