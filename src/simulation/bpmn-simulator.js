const reductor = (state = {executed: [], enabled: proc.sources}, action) => {
  switch (action.type) {
    case 'FIRE_ELEMENT':
      if (state.enabled.length > 0) {
        var toExecute = state.enabled[0];
        var toEnable = [];
        console.log(proc.adjList);
        for (let successor of proc.adjList.get(toExecute) || [])
          toEnable.push(successor);
        return {executed: [toExecute].concat(state.executed), enabled: toEnable};
      }
    default:
      return state;
  }
};

export const simulator = createStore(reductor);
