import {BPMNUtil} from '../util/bpmn-util';

class BPMNProcessInstance {
    constructor (procDef) {
        this.procDef = procDef;
        this.sequenceFlowTokens = new Map();
        for (let key of this.procDef.sequenceFlows.keys())
            this.sequenceFlowTokens.set(key, 0);
    }
}

class BPMNProcess {

    constructor () {
        this.elements = new Array();
        this.adjList = new Map();
        this.invAdjList = new Map();
        this.sequenceFlows = new Map();
        this.sources = new Array();
        this.sinks = new Array();
        this.attachments = new Map();
    }

    addFlowEdge(edge) {
        var src = edge.sourceRef;
        var tgt = edge.targetRef;

        // Let's build the adjacency list for sequenceFlow edges
        var prev = this.adjList.get(src) || [];
        this.adjList.set(src, prev.concat(tgt));
        this.invAdjList.set(tgt, (this.adjList.get(tgt) || []).concat(src));

        this.sequenceFlows.set(edge.id, edge);

        this.elements.push(src);
        this.elements.push(tgt);

        if(src.attachedToRef != null) {
          var att = src.attachedToRef;
          if(!BPMNUtil.containsElement((this.attachments.get(att) || []), src)) {
            this.attachments.set(att, (this.attachments.get(att) || []).concat(src));
          }
        }

        if(tgt.attachedToRef != null) {
          var att = tgt.attachedToRef;
          if(!BPMNUtil.containsElement((this.attachments.get(att) || []), tgt)) {
            this.attachments.set(att, (this.attachments.get(att) || []).concat(tgt));
          }
        }
    }

    pack() {
        for (let key of this.adjList.keys())
            if (!this.invAdjList.has(key) && key.attachedToRef == null
                && !this.isSubProcess(key.$parent))
                this.sources.push(key);
        for (let key of this.invAdjList.keys())
            if (!this.adjList.has(key))
                this.sinks.push(key);

        $.unique(this.elements);
    }

    isUserTask(element) {
      return element.$type.indexOf('UserTask') > -1;
    }

    isTask(element) {
      return element.$type == 'bpmn:Task';
    }

    isANDGateway(element) {
      return element.$type.indexOf('ParallelGateway') > -1;
    }

    isXORGateway(element) {
      return element.$type.indexOf('ExclusiveGateway') > -1;
    }

    isORGateway(element) {
      return element.$type.indexOf('InclusiveGateway') > -1;
    }

    isEventGateways(element) {
      return element.$type.indexOf('EventBasedGateway') > -1;
    }

    isIntermediaryEvent(element) {
      return element.$type.indexOf('Intermediate') > -1
              && element.$type.indexOf('Event') > -1;
    }

    isThrowEvent(element) {
      return element.$type.indexOf('ThrowEvent') > -1;
    }

    isMessageEvent(element) {
      if (!element.eventDefinitions) {
        return false;
      }

      for(let def of element.eventDefinitions) {
        if(def.$type.indexOf('MessageEvent') > -1) {
          return true;
        }
      }

      return false;
    }

    isStartEvent(element) {
      return element.$type.indexOf('StartEvent') > -1;
    }

    isEndEvent(element) {
      return element.$type.indexOf('EndEvent') > -1;
    }

    isBoundaryEvent(element) {
      return element.$type.indexOf('BoundaryEvent') > -1;
    }

    isErrorEvent(element) {
      if (!element.eventDefinitions) {
        return false;
      }

      for(let def of element.eventDefinitions) {
        if(def.$type.indexOf('ErrorEvent') > -1) {
          return true;
        }
      }
    }

    isCancelEvent(element) {
      if (!element.eventDefinitions) {
        return false;
      }

      for(let def of element.eventDefinitions) {
        if(def.$type.indexOf('CancelEvent') > -1) {
          return true;
        }
      }
    }

    isScriptTask(element) {
      return element.$type.indexOf('ScriptTask') > -1;
    }

    isSequenceFlow(element) {
      return element.$type.indexOf('SequenceFlow') > -1;
    }

    isSubProcess(element) {
      return element.$type.indexOf('SubProcess') > -1;
    }

    isProcess(element) {
      return element.$type === 'bpmn:Process';
    }

    isExecutableElement(element) {
      return element.$type.indexOf('Gateway') > -1
          || element.$type.indexOf('Event') > -1
          || element.$type.indexOf('Task') > -1;
    }

    isElementSelectable(state, element) {
      for(let pair of state.selectable) {
        for(let sel of pair) {
          // Sometimes the label of the element is sent instead of the element
          element = element.replace('_label', '');
          if(sel.id === element) {
            return true;
          }
        }
      }

      return false;
    }

    isElementDeselectable(state, element) {
      for (let des of state.deselectable) {
        // Sometimes the label of the element is sent instead of the element
        element = element.replace('_label', '');
        if(des.id === element) {
          return true;
        }
      }
      return false;
    }

    getElement(state, element_id) {
      var elem;
      // search for the element in the list
      for(let el of state.allElements) {
        if (el.id == element_id) {
          elem = el;
        }
      }

      return elem;
    }

    evaluateCondition(businessObject, condition) {
      if(eval('businessObject.' + condition)) {
        return true;
      }
      return false;
    }

    getStartElementsFromSubprocess(subprocess) {
      var startEvents = [];

      for(let element of subprocess.flowElements) {
        if(this.isStartEvent(element)) {
          startEvents = [element].concat(startEvents);
        }
      }

      return startEvents;
    }

    collectSubProcesses(process) {
      var subProcesses = [];
      for(let el of process.flowElements) {
        if (this.isSubProcess(el)) {
          subProcesses.push(el);
          var collectedSubProcesses = this.collectSubProcesses(el)
          subProcesses = subProcesses.concat(collectedSubProcesses);
        }
      }
      return subProcesses;
    }

    getProcess(rootElements) {
      for(let rootEl of rootElements) {
        if(this.isProcess(rootEl)) {
          return rootEl;
        }
      }
    }

    // general function for handling all elements
    fireElement(state) {
      var result = state;
      if (state.enabled.length > 0) {
        var element = state.enabled[0];
        // if it is a task open the form
        if(this.isUserTask(element)) {
          this.openTaskWindow(state, element);
        }
        else {
          result = this.handleElementExecution(state, element);
        }
      }
      console.log(result.tokens);
      return result;
    }

    handleElementExecution(state, element) {
      var newState;
      if(this.isTask(element)) {
        newState = this.fireDefault(state, element);
      }
      // User Task
      if(this.isUserTask(element)) {
        newState = this.fireDefault(state, element);
      }
      // AND Split
      else if(this.isANDGateway(element) && element.outgoing.length > 1) {
        newState = this.fireANDSplit(state, element);
      }
      // AND Join
      else if(this.isANDGateway(element) && element.outgoing.length <= 1) {
        newState = this.fireANDJoin(state, element);
      }
      // XOR Split
      else if (this.isXORGateway(element) && element.outgoing.length > 1) {
        newState = this.fireXORSplit(state, element);
      }
      // XOR Join
      else if (this.isXORGateway(element) && element.outgoing.length <= 1) {
        newState = this.fireXORJoin(state, element);
      }
      // OR Split
      else if (this.isORGateway(element) && element.outgoing.length > 1) {
        newState = this.fireORSplit(state, element);
      }
      // Event Split
      else if (this.isEventGateways(element) && element.outgoing.length > 1) {
        newState = this.fireEventSplit(state, element);
      }
      // Message Event
      else if (this.isMessageEvent(element) && !this.isThrowEvent(element)) {
        var event = new CustomEvent('eventExecuted', { 'detail': element});
        window.dispatchEvent(event);

        newState = this.fireDefault(state, element);
      }
      // Script Task
      else if (this.isScriptTask(element)) {
        var event = new CustomEvent('scriptTaskExecuted', {'detail': element});
        window.dispatchEvent(event);

        newState = this.fireDefault(state, element);
      }
      // End Event (but not error event)
      else if (this.isEndEvent(element) && !this.isErrorEvent(element)) {
        newState = this.fireEndEvent(state, element);
      }
      // Throw Event
      else if(this.isErrorEvent(element)) {
        newState = this.fireErrorEvent(state, element);
      }
      else if(this.isCancelEvent(element)) {
        newState = this.fireCancelEvent(state, element);
      }
      else {
        newState = this.fireDefault(state, element);
      }

      newState = this.postProcessingEnabledElements(newState);

      return newState;
    }

    postProcessingEnabledElements(state) {
      var newEnabled  = state.enabled.slice();
      var newBlocked = state.blocked.slice();
      var newSelectable = state.selectable.slice();
      var newTokens = state.tokens.slice();
      for(let enb of state.enabled) {
        // test gateways activation
        if((this.isANDGateway(enb) && enb.incoming.length > 1 && !this.testANDJoinActivation(enb, state))
        || (this.isORGateway(enb) && enb.incoming.length > 1 && !this.testORJoinActivation(enb, state))) {
          newEnabled = BPMNUtil.removeElement(newEnabled, enb);
          newBlocked = [enb].concat(newBlocked);
        }
        // set intermediary evets as selectable
        else if(this.isIntermediaryEvent(enb)) {
          newEnabled = BPMNUtil.removeElement(newEnabled, enb);
          newSelectable = [[enb]].concat(newSelectable);
        }
        // set tasks with events attached as selectable
        else if(this.attachments.get(enb) != null && (this.isUserTask(enb) || this.isScriptTask(enb) || this.isTask(enb))) {
          newEnabled = BPMNUtil.removeElement(newEnabled, enb);
          newSelectable = [this.attachments.get(enb).concat(enb)].concat(newSelectable);
        }
        // enter in the SubProcess
        else if(this.isSubProcess(enb)) {
          newEnabled = this.getStartElementsFromSubprocess(enb).concat(BPMNUtil.removeElement(newEnabled, enb));

          for(let inEdge of enb.incoming || []) {
            newTokens.splice(newTokens.indexOf(inEdge.id), 1);
          }

          for(let att of this.attachments.get(enb)) {
            if(this.isCancelEvent(att)) {
              newSelectable = [[att]].concat(newSelectable);
            }
          }
        }
      }

      for(let blk of state.blocked) {
        // test gatways activation
        if((this.isANDGateway(blk) && blk.incoming.length > 1 && this.testANDJoinActivation(blk, state))
        || (this.isORGateway(blk) && blk.incoming.length > 1 && this.testORJoinActivation(blk, state))) {
          newBlocked = BPMNUtil.removeElement(newBlocked, blk);
          newEnabled = [blk].concat(newEnabled);
        }
      }

      return {allElements: state.allElements, executed: state.executed, enabled: newEnabled,
              selectable: newSelectable, tokens: newTokens, blocked: newBlocked,
              deselectable: state.deselectable};
    }

    testANDJoinActivation(andJoin, state) {
      for(let pred of andJoin.incoming) {
        if(state.tokens.indexOf(pred.id) < 0) {
          return false;
        }
      }
      return true;
    }

    testORJoinActivation(orJoin, state) {
      var greenPred = [];
      var redPred = [];
      var redHash = [];
      var greenHash = [];

      for(let incomingEdge of orJoin.incoming) {
        if(BPMNUtil.containsElement(state.tokens, incomingEdge.id)) {
          redPred.push(incomingEdge);
        }
        else {
          greenPred.push(incomingEdge);
        }
      }

      // For red hash
      while(redPred.length > 0) {
        var connection = redPred.pop();
        if(!BPMNUtil.containsElement(redHash, connection.id)) {
          redHash.push(connection.id);
          if(connection.sourceRef != orJoin && connection.sourceRef.incoming) {
            for (let predEdge of connection.sourceRef.incoming) {
              if(!BPMNUtil.containsElement(redPred, predEdge) && !BPMNUtil.containsElement(redHash, predEdge.id)) {
                redPred.push(predEdge);
              }
            }
          }
        }
      }

      // For green hash
      while(greenPred.length > 0) {
        var connection = greenPred.pop();
        if(!BPMNUtil.containsElement(greenHash, connection.id)) {
          greenHash.push(connection.id);
          if(connection.sourceRef != orJoin && connection.sourceRef.incoming) {
            for(let predEdge of connection.sourceRef.incoming) {
              if(!BPMNUtil.containsElement(greenPred, predEdge)
              && !BPMNUtil.containsElement(greenHash, predEdge.id)
              && !BPMNUtil.containsElement(redHash, predEdge.id)) {
                greenPred.push(predEdge);
              }
            }
          }
        }
      }

      if(BPMNUtil.setIntersection(greenHash, state.tokens).length == 0) {
        return true;
      }

      return false;
    }

    fireDefault(state, element) {
      var toExecute = [];
      var successors = [];
      var newTokens = [];
      for (let successor of this.adjList.get(element) || []) {
        if(!BPMNUtil.containsElement(state.enabled, successor)
            && !BPMNUtil.containsElement(state.blocked, successor)) {
          successors.push(successor);
        }
      }
      toExecute.push(element);

      var toEnable = successors.concat(BPMNUtil.removeElement(state.enabled, element));

      var newTokens = state.tokens.slice();
      for(let inEdge of element.incoming || []) {
        newTokens.splice(newTokens.indexOf(inEdge.id), 1);
      }
      for(let outEdge of element.outgoing || []) {
        newTokens.push(outEdge.id);
      }

      return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
              enabled: toEnable, selectable: state.selectable.slice(), tokens: newTokens,
              blocked: state.blocked.slice(), deselectable: state.deselectable.slice()};
    }

    openTaskWindow(state, element) {
      var des = false;
      if(BPMNUtil.containsElement(state.deselectable, element)) {
        des = true;
      }
      var event = new CustomEvent('taskExecuted', { 'detail': {'state': state, 'element': element, deselectable: des}});
      window.dispatchEvent(event);
      return state;
    }

    fireANDSplit(state, element) {
      var toExecute = [];
      var successors = [];
      for (let successor of this.adjList.get(element) || []) {
        if(!BPMNUtil.containsElement(state.enabled, successor)
            && !BPMNUtil.containsElement(state.blocked, successor)) {
          successors.push(successor);
        }
      }
      toExecute.push(element);
      var toEnable = successors.concat(BPMNUtil.removeElement(state.enabled, element));
      var newTokens = state.tokens.slice();
      for(let inEdge of element.incoming || []) {
        newTokens.splice(newTokens.indexOf(inEdge.id), 1);
      }
      for(let outEdge of element.outgoing || []) {
        newTokens.push(outEdge.id);
      }
      return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
              enabled: toEnable, selectable: state.selectable.slice(), tokens: newTokens,
              blocked: state.blocked.slice(), deselectable: state.deselectable.slice()};
    }

    fireANDJoin(state, element) {
      var toExecute = [];
      var successors = [];
      var newTokens = [];
      for (let successor of this.adjList.get(element) || []) {
        if(!BPMNUtil.containsElement(state.enabled, successor)
            && !BPMNUtil.containsElement(state.blocked, successor)) {
          successors.push(successor);
        }
      }
      toExecute.push(element);
      var toEnable = successors.concat(BPMNUtil.removeElement(state.enabled, element));
      var newTokens = state.tokens.slice();
      for(let inEdge of element.incoming || []) {
        newTokens.splice(newTokens.indexOf(inEdge.id), 1);
      }
      for(let outEdge of element.outgoing || []) {
        newTokens.push(outEdge.id);
      }
      return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
              enabled: toEnable, selectable: state.selectable.slice(), tokens: newTokens,
              blocked: state.blocked.slice(), deselectable: state.deselectable.slice()};
    }

    fireXORSplit(state, element) {
      var toExecute = [];
      var toSelect = [];
      var successors = [];

      var conditionSequenceFlow;

      var businessObject = window.businessDataObject.getEditor('root').getValue();
      for(let flow of element.outgoing) {
        var conditionExt = getExtension(flow, 'be:FlowCondition');
        var conditionString = JSON.parse(conditionExt.condition).condition;
        if(conditionString && this.evaluateCondition(businessObject, conditionString)) {
          conditionSequenceFlow = flow;
          break;
        }
      }

      // the condition for one element was evaluated to true
      if(conditionSequenceFlow != null) {
        toExecute.push(element);
        var toEnable = BPMNUtil.removeElement(state.enabled, element);
        toEnable = [conditionSequenceFlow.targetRef].concat(toEnable);
        var newTokens = state.tokens.slice();
        for(let inEdge of element.incoming || []) {
          newTokens.splice(newTokens.indexOf(inEdge.id), 1);
        }
        newTokens.push(conditionSequenceFlow.id);
        return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
                enabled: toEnable, selectable: state.selectable.slice(), tokens: newTokens,
                blocked: state.blocked.slice(), deselectable: state.deselectable.slice()};
      }
      // no condition was evaluated to true
      else {
        for (let successor of this.adjList.get(element) || []) {
          if(!BPMNUtil.containsElement(state.enabled, successor)
              && !BPMNUtil.containsElement(state.blocked, successor)) {
            successors.push(successor);
          }
        }
        toExecute.push(element);
        var toEnable = BPMNUtil.removeElement(state.enabled, element);
        var toSelect = state.selectable.concat([successors]);
        var newTokens = state.tokens.slice();
        for(let inEdge of element.incoming || []) {
          newTokens.splice(newTokens.indexOf(inEdge.id), 1);
        }
        for(let outEdge of element.outgoing || []) {
          newTokens.push(outEdge.id);
        }
        return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
                enabled: toEnable, selectable: toSelect, tokens: newTokens,
                blocked: state.blocked.slice(), deselectable: state.deselectable.slice()};
      }
    }

    fireXORJoin(state, element) {
      var toExecute = [];
      var successors = [];
      var newTokens = [];
      for (let successor of this.adjList.get(element) || []) {
        if(!BPMNUtil.containsElement(state.enabled, successor)
            && !BPMNUtil.containsElement(state.blocked, successor)) {
          successors.push(successor);
        }
      }
      toExecute.push(element);
      var toEnable = successors.concat(BPMNUtil.removeElement(state.enabled, element));
      var newTokens = state.tokens.slice();
      for(let outEdge of element.outgoing || []) {
        newTokens.push(outEdge.id);
      }
      for(let inEdge of element.incoming || []) {
        // Remove tokens just from one predecessor at a time
        if(BPMNUtil.containsElement(newTokens, inEdge.id)) {
            newTokens.splice(newTokens.indexOf(inEdge.id), 1);
            return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
                    enabled: toEnable, selectable: state.selectable.slice(), tokens: newTokens,
                    blocked: state.blocked.slice(), deselectable: state.deselectable.slice()};
        }
      }
      return state;
    }

    fireORSplit(state, element) {
      var toExecute = [];
      var successors = [];
      for (let successor of this.adjList.get(element) || []) {
        if(!BPMNUtil.containsElement(state.enabled, successor)
            && !BPMNUtil.containsElement(state.blocked, successor)) {
          successors.push(successor);
        }
      }
      toExecute.push(element);
      var toEnable = BPMNUtil.removeElement(state.enabled, element);
      var newDeselectable = state.deselectable.concat(successors);
      var newTokens = state.tokens.slice();
      for(let inEdge of element.incoming || []) {
        newTokens.splice(newTokens.indexOf(inEdge.id), 1);
      }
      for(let outEdge of element.outgoing || []) {
        newTokens.push(outEdge.id);
      }
      return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
              enabled: toEnable, selectable: state.selectable.slice(), tokens: newTokens,
              blocked: state.blocked.slice(), deselectable: newDeselectable};
    }

    fireEventSplit(state, element) {
      return this.fireXORSplit(state, element);
    }

    fireEndEvent(state, element) {
      if(this.isSubProcess(element.$parent)) {
        var subProc = element.$parent;

        var toExecute = [];
        var successors = [];
        var toSelect = state.selectable.slice();
        var newTokens = state.tokens.slice();
        for (let successor of this.adjList.get(subProc) || []) {
          if(!BPMNUtil.containsElement(state.enabled, successor)
              && !BPMNUtil.containsElement(state.blocked, successor)) {
            successors.push(successor);
          }
        }
        toExecute.push(element);

        var toEnable = successors.concat(BPMNUtil.removeElement(state.enabled, element));

        for(let inEdge of element.incoming || []) {
          newTokens.splice(newTokens.indexOf(inEdge.id), 1);
        }
        for(let outEdge of subProc.outgoing || []) {
          newTokens.push(outEdge.id);
        }

        // Test if the sub process has cancel events attached and deactivate them
        for(let attEl of this.attachments.get(subProc)) {
          if(this.isCancelEvent(attEl)) {
            for(let pair of state.selectable) {
              for(let el of pair) {
                if(el.id == attEl.id) {
                  toSelect = BPMNUtil.removeElement(toSelect, pair);
                }
              }
            }
          }
        }

        return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
                enabled: toEnable, selectable: toSelect, tokens: newTokens, blocked: state.blocked.slice(),
                deselectable: state.deselectable.slice()};
      }
      else {
        return this.fireDefault(state, element);
      }
    }

    fireErrorEvent(state, element) {
      if(this.isSubProcess(element.$parent)) {
        var subProc = element.$parent;
        var att = this.attachments.get(subProc);

        var toExecute = [];
        var successors = [];
        var toSelect = state.selectable.slice();
        var newTokens = state.tokens.slice();

        if(att != null) {
          for(let attEl of att) {
            if(this.isErrorEvent(attEl) && attEl.name === element.name) {
              successors.push(attEl);
            }
          }
        }
        toExecute.push(element);

        var toEnable = successors.concat(BPMNUtil.removeElement(state.enabled, element));

        for(let inEdge of element.incoming || []) {
          newTokens.splice(newTokens.indexOf(inEdge.id), 1);
        }

        // Test if the sub process has cancel events attached and deactivate them
        for(let attEl of this.attachments.get(subProc)) {
          if(this.isCancelEvent(attEl)) {
            for(let pair of state.selectable) {
              for(let el of pair) {
                if(el.id == attEl.id) {
                  toSelect = BPMNUtil.removeElement(toSelect, pair);
                }
              }
            }
          }
        }

        return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
                enabled: toEnable, selectable: toSelect, tokens: newTokens,
                blocked: state.blocked.slice(), deselectable: state.deselectable.slice()};
      }
      else {
        return this.fireDefault(state, element);
      }
    }

    fireCancelEvent(state, element) {
      var subProc = element.attachedToRef;

      if(subProc != null) {
        var toExecute = [];
        var successors = [];
        var newTokens = state.tokens.slice();
        var toEnable = state.enabled.slice();
        var toSelect = state.selectable.slice();
        var toBlock = state.blocked.slice();

        // Remove sub process elements from enabled
        for(let enbEl of state.enabled) {
          var parentElement = enbEl.$parent;
          while(!this.isProcess(parentElement)) {
            if(parentElement == subProc) {
              // Remove the tokens from the incoming flows and remove these
              // elements in the new enabled list
              if(enbEl.incoming) {
                for(let incomingFlow of enbEl.incoming) {
                  newTokens = BPMNUtil.removeElement(newTokens, incomingFlow.id);
                }
              }

              toEnable = BPMNUtil.removeElement(toEnable, enbEl)
              break;
            }
            parentElement = parentElement.$parent;
          }
        }

        // Remove sub process elements from selectable
        for(let selectionPair of state.selectable) {
          var remove = true;

          for(let selectionElement of selectionPair) {
            var parentElement = selectionElement.$parent;
            while(!this.isProcess(parentElement)) {
              if(selectionElement.$parent != subProc) {
                remove = false;
                break;
              }
              parentElement = parentElement.$parent;
            }
          }

          if(remove) {
            toSelect = BPMNUtil.removeElement(toSelect, selectionPair);

            console.log(selectionPair);
            for(let selectionElement of selectionPair) {
              if(selectionElement.incoming != null) {
                for(let incomingFlow of selectionElement.incoming) {
                  newTokens = BPMNUtil.removeElement(newTokens, incomingFlow.id);
                }
              }
            }
          }
        }

        // Remove sub process elements from blocked
        for(let blkEl of state.blocked) {
          var parentElement = blkEl.$parent;
          while(!this.isProcess(parentElement)) {
            if(parentElement == subProc) {
              // Remove the tokens from the incoming flows and remove these
              // elements in the new blocked list
              for(let incomingFlow of blkEl.incoming) {
                newTokens = BPMNUtil.removeElement(newTokens, incomingFlow.id);
              }
              toBlock = BPMNUtil.removeElement(toBlock, blkEl);
              break;
            }
            parentElement = parentElement.$parent;
          }
        }

        for(let outEdge of element.outgoing || []) {
          newTokens.push(outEdge.id);
        }

        for (let successor of this.adjList.get(element) || []) {
          if(!BPMNUtil.containsElement(state.enabled, successor)
              && !BPMNUtil.containsElement(state.blocked, successor)) {
            successors.push(successor);
          }
        }
        toEnable = successors.concat(toEnable);

        toExecute.push(element);

        return {allElements: state.allElements.slice(), executed: toExecute.concat(state.executed.slice()),
                enabled: toEnable, selectable: toSelect, tokens: newTokens, blocked: toBlock,
                deselectable: state.deselectable.slice()};
      }
      else {
        return this.fireDefault(state, element);
      }
    }

    selectElement(state, element_id, selection) {
      if (selection == 'selectable') {
        for(let pair of state.selectable) {
          for(let sel of pair) {
            if(sel.id == element_id) {
              var result = this.handleElementExecution(state, sel);

              var toSelect = BPMNUtil.removeElement(result.selectable, pair);
              var toDeselect = BPMNUtil.removeElement(pair, sel);

              var newTokens = result.tokens.slice();
              for(let desEl of toDeselect) {
                for(let inEdge of desEl.incoming || []) {
                  newTokens = BPMNUtil.removeElement(newTokens, inEdge.id);
                }
              }
              console.log(newTokens);

              return {allElements: result.allElements, executed: result.executed, enabled: result.enabled,
                selectable: toSelect, tokens: newTokens, blocked: result.blocked, deselectable: result.deselectable};
            }
          }
        }
      }
      else if(selection == 'deselectable') {
        for(let des of state.deselectable) {
          if(des.id == element_id) {
            var result = this.handleElementExecution(state, des);

            var toDeselect = BPMNUtil.removeElement(state.deselectable, des);

            console.log(result.tokens);

            return {allElements: result.allElements, executed: result.executed, enabled: result.enabled,
              selectable: result.selectable, tokens: result.tokens, blocked: result.blocked, deselectable: toDeselect};
          }
        }
      }
      return state;
    }

    deselectElement(state, element) {
      if(BPMNUtil.containsElement(state.deselectable, element)) {
        var newTokens = state.tokens.slice();
        for(let inEdge of element.incoming || []) {
          newTokens.splice(newTokens.indexOf(inEdge.id), 1);
        }

        var newState =  {allElements: state.allElements.slice(), executed: state.executed.slice(),
                enabled: state.enabled.slice(), selectable: state.selectable.slice(),
                tokens: newTokens, blocked: state.blocked.slice(),
                deselectable: BPMNUtil.removeElement(state.deselectable, element)};

        newState = this.postProcessingEnabledElements(newState);

        console.log(newTokens);

        return newState;
      }
      return state;
    }
}

module.exports = {
  BPMNProcess: BPMNProcess,
  BPMNProcessInstance: BPMNProcessInstance
};
