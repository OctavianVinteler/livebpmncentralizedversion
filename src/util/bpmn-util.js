class BPMNUtil {
  static removeElement(array, element) {
    if(this.containsElement(array, element)) {
      return array.slice(0, array.indexOf(element))
            .concat(array.slice(array.indexOf(element) + 1, array.length));
    }
    else {
      return array;
    }

  }

  static containsElement(array, element) {
    return array.indexOf(element) >= 0;
  }

  static setIntersection(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        if (b.indexOf(e) !== -1) return true;
    });
  }

  static clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
  }

  static testArrayEquality(array1, array2) {
    if(array1.length != array2.length) {
      return false;
    }

    for(let el of array1) {
      if(array2.indexOf(el) < 0) {
        return false;
      }
    }

    return true;
  }
}

module.exports = {
  BPMNUtil: BPMNUtil
};
