import BpmnViewer from 'bpmn-js';
import BpmnModdle from 'bpmn-moddle';
import {createStore} from 'redux';
import {BPMNProcess} from './semantics/bpmn-semantics';
import $ from 'jquery';
import {BPMNUtil} from './util/bpmn-util';
import {SimulationEditor} from './ui/simulationEditor';

var bePackage = require('../resources/be.json');
var viewer = new BpmnViewer({ container: document.getElementById("canvas"),
                              moddleExtensions: {
                                be: bePackage
                              }});

function removeMarkers(element, canvas) {
  canvas.removeMarker(element, 'enabled');
  canvas.removeMarker(element, 'executed');
  canvas.removeMarker(element, 'selectable');
  canvas.removeMarker(element, 'blocked');
}

function redefineTaskForm(store, proc) {
  var allElements = store.getState().allElements;

  for(let el of allElements) {
    if(proc.isUserTask(el)) {
      var extension = getExtension(el, 'be:BusinessTask');
      if(!BPMNUtil.testArrayEquality(Object.keys(JSON.parse(extension.activationSchema)),
                                    Object.keys(window.formTemplate))) {
        extension.activationSchema = JSON.stringify(window.formTemplate);
      }
    }
  }
}

function openUserTaskWindow(detail, editor) {
  // Get the activation properties for this particular task
  var extension = getExtension(detail.element, 'be:BusinessTask');
  var activationSchema = JSON.parse(extension.activationSchema);
  editor.activateTaskProperties(activationSchema, window.businessDataObject);

  window.activateEditor(detail);
}

function destroyEditors() {
  if(window.flowSequnceEditor) {
    window.flowSequnceEditor.destroy();
  }
  if(window.businessDataObject) {
    window.businessDataObject.destroy();
  }
  if(window.taskSchemaObject) {
    window.taskSchemaObject.destroy();
  }
  if(window.scriptTaskEditor) {
    window.scriptTaskEditor.destroy();
  }
  if(window.temp) {
    window.temp.destroy();
  }
  if(window.messageEventData) {
    window.messageEventData.destroy();
  }
  if(window.messageEventEditor) {
    window.messageEventEditor.destroy();
  }
}

function registerFileDrop(container, callback) {
  function handleFileSelect(e) {
    e.stopPropagation();
    e.preventDefault();

    var files = e.dataTransfer.files;
    var file = files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      var xml = e.target.result;
      callback(xml, "{}");
    };
    reader.readAsText(file);
  }

  function handleDragOver(e) {
    e.stopPropagation();
    e.preventDefault();

    e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  }

  container.addEventListener('dragover', handleDragOver, false);
  container.addEventListener('drop', handleFileSelect, false);
}

function openDiagram(diagramXML, businessSchema, flows = null, elements = null) {
  var moddle = new BpmnModdle([bePackage]);
  destroyEditors();
  moddle.fromXML(diagramXML, function(err, definitions) {

    // Preprocessing of XML file
    //var currentBusinessSchema = businessSchema;
    var proc = new BPMNProcess();
    // If diagram contains pools and lanes, find the process
    var procDef = proc.getProcess(definitions.rootElements);
    var seqFlowEdges = procDef.flowElements.filter(function(elem){
      return proc.isSequenceFlow(elem);
    });

    // Get all the sub processes
    var subProcesses = proc.collectSubProcesses(procDef);

    // Include the sequence -flows in the subprocesses in the edge set as well
    for(let subProc of subProcesses) {
      for(let subEl of subProc.flowElements) {
        if(proc.isSequenceFlow(subEl)) {
          seqFlowEdges.push(subEl);
        }
      }
    }

    for (let edge of seqFlowEdges)
      proc.addFlowEdge(edge);
    proc.pack();

    var editor = new SimulationEditor();

    var defineTaskSchemaButton = document.getElementById('define-task-schema');
    $(defineTaskSchemaButton).unbind();
    defineTaskSchemaButton.disabled = false;

    var startButton = document.getElementById('start-simulation');
    $(startButton).unbind();
    startButton.disabled = false;
    $(startButton).bind('click', function() {
      editor.simulationStarted = true;
      startButton.disabled = true;
      defineTaskSchemaButton.disabled = true;

      var taskSchema = JSON.parse(window.taskSchemaObject.getEditor('root').getValue()['json-representation']);
      var inputSchema = editor.processTaskSchema(taskSchema);
      window.setBusinessEditor(inputSchema);
      window.setMessageEventData(inputSchema);
    });

    $(defineTaskSchemaButton).bind('click', function() {
      window.openTaskSchemaForm(editor.selectPropertiesFromSchema);
    });

    var sendBugButton = document.getElementById('send-feedback');
    $(sendBugButton).unbind();
    $(sendBugButton).bind('click', function() {
      window.openBugWindow(diagramXML);
    });

    var downloadDiagramButton = document.getElementById('download-diagram');
    $(downloadDiagramButton).unbind();
    $(downloadDiagramButton).bind('click', function() {
      var data = {};
      var flowKeys = proc.sequenceFlows.keys();
      var flowArray = [];
      for(let fk of flowKeys) {
        flowArray.push(proc.sequenceFlows.get(fk));
      }
      data.elements = store.getState().allElements;
      data.flows = flowArray;
      data.businessData = window.taskSchemaObject.getEditor('root.json-representation').getValue();
      data.diagram = diagramXML;
      var a = document.getElementById("a");
      var blob = new Blob([JSON.stringify(data)], {type: "application/json"});
      a.href = URL.createObjectURL(blob);
      a.download = "diagram.lbee";
      a.click();
    });

    window.setTaskSchema({
      "type": "object",
      "properties": {
        "json-representation": {
          "type": "string",
          "media": {
            "type": "application/json"
          }
        }
      }
    });

    window.setScriptTaskEditor();
    window.setFlowSequenceEditor();

    // Set the default value for the task schema as an empty json object
    window.taskSchemaObject.getEditor('root.json-representation').setValue(businessSchema);
    window.taskSchema = JSON.parse(editor.selectPropertiesFromSchema(JSON.parse(window.taskSchemaObject.getEditor('root').getValue()['json-representation'])));
    window.setTaskEditor(window.taskSchema);

    window.setMessageEventEditor(window.taskSchema);

    const reductor = (state = {allElements: proc.elements, executed: [],
                                enabled: proc.sources, selectable: [],
                                tokens: [], blocked: [], deselectable: []},
                                action) => {
      switch (action.type) {
        case 'FIRE_ELEMENT':
          if (state.enabled.length > 0) {
            const newState = proc.fireElement(state);
            return newState;
          }
          return state;
        case 'SELECT_ELEMENT':
          if(state.selectable.length > 0 || state.deselectable.length > 0) {
            var elem = proc.getElement(state, action.element);

            const newState = proc.selectElement(state, action.element, action.selection);
            return newState;
          }
          return state;
        case 'PROCEED_ELEMENT':
          if(BPMNUtil.containsElement(state.enabled, action.element)) {
            const newState = proc.handleElementExecution(state, action.element);
            return newState;
          }
          else if(proc.isElementSelectable(state, action.element.id)) {
            const newState = proc.selectElement(state, action.element.id, 'selectable');
            return newState;
          }
          else if(BPMNUtil.containsElement(state.deselectable, action.element)) {
            const newState = proc.selectElement(state, action.element.id, 'deselectable');
            return newState;
          }
          return state;
        case 'DISABLE_ELEMENT':
          var newState = proc.deselectElement(state, action.element);
          return newState;
        default:
          return state;
      }
    };

    const store = createStore(reductor);

    // initialize the extensions for all elements
    for(let el of store.getState().allElements) {
      el.extensionElements = moddle.create('bpmn:ExtensionElements');
      el.extensionElements.get('values');

      if(proc.isUserTask(el)) {
        var schema = moddle.create('be:BusinessTask');
        el.extensionElements.get('values').push(schema);
        schema.activationSchema = "{}";
      }
      else if(proc.isMessageEvent(el)) {
        var schema = moddle.create('be:MessageEvent');
        el.extensionElements.get('values').push(schema);
        schema.message = JSON.stringify(window.messageEventTemplate);
      }
      else if(proc.isScriptTask(el)) {
        var schema = moddle.create('be:ScriptTask');
        el.extensionElements.get('values').push(schema);
        schema.script = JSON.stringify(window.scriptTaskTemplate);
      }

      if(elements) {
        var receivedElement = elements.filter(function(v) {
            return v.id === el.id; // Filter out the appropriate one
        })[0];

        if(receivedElement) {
          if(proc.isUserTask(el)) {
            var activationSchemaExt = getExtension(el, 'be:BusinessTask');
            var receivedActivationSchemaExt = getExtension(receivedElement, 'be:BusinessTask');
            activationSchemaExt.activationSchema = receivedActivationSchemaExt.activationSchema;
          }
          else if(proc.isMessageEvent(el)) {
            var messageExt = getExtension(el, 'be:MessageEvent');
            var receivedMessageExt = getExtension(receivedElement, 'be:MessageEvent');
            messageExt.message = receivedMessageExt.message;
          }
          else if(proc.isScriptTask(el)) {
            var scriptExt = getExtension(el, 'be:ScriptTask');
            var receivedScriptExt = getExtension(receivedElement, 'be:ScriptTask');
            scriptExt.script = receivedScriptExt.script;
          }
        }
      }
    }

    // initialize the extensions for all flow sequenceFlows
    for(let flow_map of proc.sequenceFlows) {
      var flow = flow_map[1];
      flow.extensionElements = moddle.create('bpmn:ExtensionElements');
      flow.extensionElements.get('values');

      if(proc.isSequenceFlow(flow)) {
        var schema = moddle.create('be:FlowCondition');
        flow.extensionElements.get('values').push(schema);
        schema.condition = JSON.stringify(window.flowSequenceTemplate);
      }

      // If the elements were received from uploading diagrams, copy the extensions
      if(flows) {
        var receivedElement = flows.filter(function(v) {
            return v.id === flow.id; // Filter out the appropriate one
        })[0];

        if(receivedElement) {
          var conditionExt = getExtension(flow, 'be:FlowCondition');
          var receivedConditionExt = getExtension(receivedElement, 'be:FlowCondition');
          conditionExt.condition = receivedConditionExt.condition;
        }
      }
    }

    // Updating UI due to changes in state
    viewer.importDefinitions(definitions, (err) => {
      var canvas = viewer.get('canvas');
      const render = () => {
        for (let element of store.getState().allElements) {
          removeMarkers(element, canvas);
        }
        for (let element of store.getState().executed) {
          canvas.addMarker(element, 'executed');
        }
        for (let element of store.getState().enabled){
          canvas.addMarker(element, 'enabled');
        }
        for (let pair of store.getState().selectable) {
          for(let element of pair) {
            canvas.addMarker(element, 'selectable');
          }
        }
        for (let element of store.getState().deselectable) {
          canvas.addMarker(element, 'selectable');
        }
        for (let element of store.getState().blocked) {
          canvas.addMarker(element, 'blocked');
        }
      };
      store.subscribe(render);
      render();

      var eventBus = viewer.get('eventBus');

      // UI events
      eventBus.on('element.click', (event) => {
        if(editor.simulationStarted) {
          if(proc.isElementSelectable(store.getState(), event.element.id)) {
            store.dispatch({type: 'SELECT_ELEMENT', element: event.element.id, selection: 'selectable', elType: event.element.type});
          }
          else if(proc.isElementDeselectable(store.getState(), event.element.id)) {
            // If it is User task, the window is different
            if(proc.isUserTask(event.element.businessObject)) {
              openUserTaskWindow({'state': store.getState(), 'element': event.element.businessObject, 'deselectable': true},
                                  editor);
            }
            else {
              window.openEnablementWindow({element: event.element.businessObject, state: store.getState()});
            }
          }
          else {
            store.dispatch({type: 'FIRE_ELEMENT'});
          }
        }
        else {
          if(proc.isUserTask(event.element.businessObject)) {
            window.openTaskForm(event.element.businessObject, moddle);
          }
          else if(proc.isMessageEvent(event.element.businessObject)) {
            var messageExt = getExtension(event.element.businessObject, 'be:MessageEvent');
            window.messageEventEditor.getEditor('root').setValue(JSON.parse(messageExt.message));
            window.openMessageEventForm(event.element.businessObject, moddle);
          }
          else if(proc.isScriptTask(event.element.businessObject)) {
            var scriptExt = getExtension(event.element.businessObject, 'be:ScriptTask');
            window.scriptTaskEditor.getEditor('root').setValue(JSON.parse(scriptExt.script));
            window.openScriptTaskForm(event.element.businessObject, moddle);
          }
          else if(proc.isSequenceFlow(event.element.businessObject)) {
            var conditionExt = getExtension(event.element.businessObject, 'be:FlowCondition');
            window.flowSequnceEditor.getEditor('root').setValue(JSON.parse(conditionExt.condition));
            window.openFlowConditionForm(event.element.businessObject, moddle);
          }
        }
      });

      $(window).unbind();

      $(window).bind('taskExecuted', (event) => {
        openUserTaskWindow(event.detail, editor);
      });

      $(window).bind('eventExecuted', (event) => {
        var messageExt = getExtension(event.detail, 'be:MessageEvent');
        var activationSchema = JSON.parse(messageExt.message);
        // if the activation schema is empty, don't show the form
        if(JSON.stringify(activationSchema) != JSON.stringify({})) {
          editor.activateTaskProperties(activationSchema, window.messageEventData);
          window.displayEventMessage(event.detail);
        }
      });

      $(window).bind('scriptTaskExecuted', (event) => {
        var scriptExt = getExtension(event.detail, 'be:ScriptTask');
        var scriptString = JSON.parse(scriptExt.script).script;

        if(scriptString) {
          var businessObject = window.businessDataObject.getEditor('root').getValue();
          try {
            eval('businessObject.' + scriptString);
            window.businessDataObject.setValue(businessObject);
          } catch (e) {
            console.log('Invalid Script');
          }

        }
      });

      $(window).bind('proceedSimulation', (event) => {
        store.dispatch({type: 'PROCEED_ELEMENT', state: event.detail.state,
                        element: event.detail.element});
      });

      $(window).bind('resetTaskActivationForm', (event) => {
        redefineTaskForm(store, proc);
      });

      $(window).bind('enableElement', (event) => {
        store.dispatch({type: 'SELECT_ELEMENT', element: event.detail.element.id,
                        selection: 'deselectable', elType: event.detail.element.$type});
      });

      $(window).bind('disableElement', (event) => {
        store.dispatch({type: 'DISABLE_ELEMENT', state: event.detail.state,
                        element: event.detail.element});
      });
    });
  });
}

var uploadDiagramButton = document.getElementById('upload-diagram');
$(uploadDiagramButton).unbind();
$(uploadDiagramButton).bind('click', function() {
  var fileInput = document.getElementById('file-upload');
  $(fileInput).val("");
  fileInput.onchange = function(event) {
    var reader = new FileReader();
    reader.onload = function(e) {
      var contents = JSON.parse(e.target.result);
      var xml = contents.diagram;
      openDiagram(xml, contents.businessData, contents.flows, contents.elements);
    };
    reader.readAsText(fileInput.files[0]);
  }
  fileInput.click();
});

if (!window.FileList || !window.FileReader) {
  window.alert(
    'Looks like you use an older browser that does not support drag and drop. ' +
    'Try using Chrome, Firefox or the Internet Explorer > 10.');
} else {
  registerFileDrop(document, openDiagram);
}
