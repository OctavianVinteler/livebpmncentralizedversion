JSONEditor.defaults.theme = 'bootstrap3';
// JSONEditor.plugins.ace.theme = 'Chrome';

window.setBusinessEditor = function(inputSchema) {
  window.businessDataObject = new JSONEditor(document.getElementById('editor_holder'), {
    schema: inputSchema,
    ajax: false,
    disable_edit_json: true,
    disable_properties: true,
    disable_collapse: true,
    required_by_default: true
  });
};

window.activateEditor = function(details) {
  $("#myModal").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('modal-save-changes');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('modal-save-changes').addEventListener('click',function() {
    var errors = window.businessDataObject.validate();
    if (errors.length) {
      console.log("ERROR!");
      console.log(errors);
    } else {
      var event = new CustomEvent('proceedSimulation', {'detail': details});
      window.dispatchEvent(event);
      $("#myModal").modal("hide");
    }
  });

  // to remove all event listeners
  oldButton = document.getElementById('modal-discard-changes');
  newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('modal-discard-changes').addEventListener('click',function() {
    $("#myModal").modal("hide");
  });

  // to remove all event listeners
  oldButton = document.getElementById('modal-deactivate');
  newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);

  if(details.deselectable) {
    document.getElementById('modal-deactivate').style.display = 'inline';
    document.getElementById('modal-deactivate').addEventListener('click',function() {
      var event = new CustomEvent('disableElement', {'detail': details});
      window.dispatchEvent(event);
      $("#myModal").modal("hide");
    });
  }
  else {
    document.getElementById('modal-deactivate').style.display = 'none';
  }

};
