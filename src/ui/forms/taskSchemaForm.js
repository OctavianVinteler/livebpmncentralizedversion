JSONEditor.defaults.theme = 'bootstrap3';
// JSONEditor.plugins.ace.theme = 'Chrome';

window.setTaskSchema = function(inputSchema) {
  window.taskSchemaObject = new JSONEditor(document.getElementById('task-schema-holder'), {
    schema: inputSchema,
    ajax: false,
    disable_edit_json: true,
    disable_properties: true,
    disable_collapse: true,
    required_by_default: true
  });
};

window.openTaskSchemaForm = function(callbackFunction) {
  $("#taskSchemaWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('task-schema-save');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('task-schema-save').addEventListener('click',function() {
    var errors = window.taskSchemaObject.validate();
    if (errors.length) {
      console.log("ERROR!");
      console.log(errors);
    } else {
      if(window.temp) {
        window.temp.destroy();
      }
      if(window.messageEventEditor) {
        window.messageEventEditor.destroy();
      }
      window.taskSchema = JSON.parse(callbackFunction(JSON.parse(window.taskSchemaObject.getEditor('root').getValue()['json-representation'])));
      window.setTaskEditor(window.taskSchema);
      window.setMessageEventEditor(window.taskSchema);

      // send an event dor reseting the schemas of all the tasks
      var event = new CustomEvent('resetTaskActivationForm');
      window.dispatchEvent(event);
      $("#taskSchemaWindow").modal("hide");
    }
  });
};
