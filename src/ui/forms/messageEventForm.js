JSONEditor.defaults.theme = 'bootstrap3';

window.setMessageEventEditor = function(inputSchema) {
  window.messageEventEditor = new JSONEditor(document.getElementById('message-event-holder'), {
    schema: inputSchema,
    ajax: false,
    disable_edit_json: true,
    disable_properties: true,
    disable_collapse: true,
    required_by_default: true
  });

  window.messageEventTemplate = JSON.parse(JSON.stringify(window.messageEventEditor.getEditor('root').getValue()));
};

window.openMessageEventForm = function(element, moddle) {
  $("#messageEventWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('message-event-save');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('message-event-save').addEventListener('click',function() {
    var errors = window.messageEventEditor.validate();
    if (errors.length) {
      console.log("ERROR!");
      console.log(errors);
    }
    else {
      var messageExt = getExtension(element, 'be:MessageEvent');
      messageExt.message = JSON.stringify(window.messageEventEditor.getEditor('root').getValue());

      $("#messageEventWindow").modal("hide");
    }
  });
};
