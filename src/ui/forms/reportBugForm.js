JSONEditor.defaults.theme = 'bootstrap3';

window.openBugWindow = function(xml_diagram) {
  $("#reportBugWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('send-bug');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('send-bug').addEventListener('click',function() {
    var text = document.getElementById('bug-text').value;
    emailjs.send("yahoo","bugemail",{bug_text: text, diagram: xml_diagram});
    document.getElementById('bug-text').value = '';
    $("#reportBugWindow").modal("hide");
  });

  // to remove all event listeners
  var oldButton = document.getElementById('cancel-bug');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('cancel-bug').addEventListener('click',function() {
    document.getElementById('bug-text').value = '';
    $("#reportBugWindow").modal("hide");
  });
};
