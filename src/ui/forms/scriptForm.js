JSONEditor.defaults.theme = 'bootstrap3';

window.setScriptTaskEditor = function() {
  window.scriptTaskEditor = new JSONEditor(document.getElementById('script-task-holder'), {
    schema: {
      "type": "object",
      "properties": {
        "script": {
          "type": "string",
          "required": "true",
          "format": "textarea"
        }
      }
    },
    ajax: false,
    disable_edit_json: true,
    disable_properties: true,
    disable_collapse: true,
    required_by_default: true
  });

  window.scriptTaskTemplate = JSON.parse(JSON.stringify(window.scriptTaskEditor.getEditor('root').getValue()));
};

window.openScriptTaskForm = function(element, moddle) {
  $("#scriptTaskWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('script-task-save');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('script-task-save').addEventListener('click',function() {
    var errors = window.scriptTaskEditor.validate();
    if (errors.length) {
      console.log("ERROR!");
      console.log(errors);
    }
    else {
      var scriptExt = getExtension(element, 'be:ScriptTask');
      scriptExt.script = JSON.stringify(window.scriptTaskEditor.getEditor('root').getValue());

      $("#messageEventWindow").modal("hide");
    }
  });
};
