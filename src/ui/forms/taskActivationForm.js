JSONEditor.defaults.theme = 'bootstrap3';
// JSONEditor.plugins.ace.theme = 'Chrome';

window.setTaskEditor = function(inputSchema) {
  window.temp = new JSONEditor(document.getElementById('task-form-holder'), {
    schema: inputSchema,
    ajax: false,
    disable_edit_json: true,
    disable_properties: true,
    disable_collapse: true,
    required_by_default: true
  });

  window.formTemplate = JSON.parse(JSON.stringify(window.temp.getEditor('root').getValue()));
};

function getExtension(element, type) {
  if (!element.extensionElements) {
    return null;
  }

  return element.extensionElements.values.filter(function(e) {
    return e.$type === type;
  })[0];
}

window.openTaskForm = function(element, moddle) {
  var schema = getExtension(element, 'be:BusinessTask');
  window.temp.getEditor('root').setValue(JSON.parse(schema.activationSchema));

  $("#taskFormWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('task-form-save');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('task-form-save').addEventListener('click',function() {
    var errors = window.taskSchemaObject.validate();
    if (errors.length) {
      console.log("ERROR!");
      console.log(errors);
    } else {
      schema.activationSchema = JSON.stringify(window.temp.getEditor('root').getValue());
      $("#taskFormWindow").modal("hide");
    }
  });
};
