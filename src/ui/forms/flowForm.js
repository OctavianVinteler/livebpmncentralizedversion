JSONEditor.defaults.theme = 'bootstrap3';

window.setFlowSequenceEditor = function() {
  window.flowSequnceEditor = new JSONEditor(document.getElementById('flow-sequence-holder'), {
    schema: {
      "type": "object",
      "properties": {
        "condition": {
          "type": "string",
          "required": "true",
          "format": "textarea"
        }
      }
    },
    ajax: false,
    disable_edit_json: true,
    disable_properties: true,
    disable_collapse: true,
    required_by_default: true
  });

  window.flowSequenceTemplate = JSON.parse(JSON.stringify(window.flowSequnceEditor.getEditor('root').getValue()));
};

window.openFlowConditionForm = function(element, moddle) {
  $("#flowSequenceWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('flow-sequence-save');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('flow-sequence-save').addEventListener('click',function() {
    var errors = window.scriptTaskEditor.validate();
    if (errors.length) {
      console.log("ERROR!");
      console.log(errors);
    }
    else {
      var conditionExt = getExtension(element, 'be:FlowCondition');
      conditionExt.condition = JSON.stringify(window.flowSequnceEditor.getEditor('root').getValue());

      $("#flowSequenceWindow").modal("hide");
    }
  });
};
