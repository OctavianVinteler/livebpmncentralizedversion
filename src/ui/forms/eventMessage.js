JSONEditor.defaults.theme = 'bootstrap3';

window.setMessageEventData = function(inputSchema) {
  window.messageEventData = new JSONEditor(document.getElementById('message-displayer'), {
    schema: inputSchema,
    ajax: false,
    disable_edit_json: true,
    disable_properties: true,
    disable_collapse: true,
    required_by_default: true
  });
}

window.displayEventMessage = function(element) {
  console.log(window.businessDataObject.getEditor('root'));
  console.log(window.businessDataObject.getValue());
  window.messageEventData.setValue(JSON.parse(JSON.stringify(window.businessDataObject.getValue())));

  $("#displayMessageWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('message-ok');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);

  document.getElementById('message-ok').addEventListener('click',function() {
    var errors = window.businessDataObject.validate();
    if (errors.length) {
      console.log("ERROR!");
      console.log(errors);
    } else {
      window.businessDataObject.setValue(JSON.parse(JSON.stringify(window.messageEventData.getValue())));
      $("#myModal").modal("hide");
    }
  });
};
