JSONEditor.defaults.theme = 'bootstrap3';

window.openEnablementWindow = function(detail) {
  $("#deselectWindow").modal({backdrop: "static", keyboard: false});

  // to remove all event listeners
  var oldButton = document.getElementById('element-enable');
  var newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('element-enable').addEventListener('click',function() {
    var event = new CustomEvent('enableElement', {'detail': detail});
    window.dispatchEvent(event);
    $("#deselectWindow").modal("hide");
  });

  // to remove all event listeners
  oldButton = document.getElementById('element-disable');
  newButton = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(newButton, oldButton);
  document.getElementById('element-disable').addEventListener('click',function() {
    var event = new CustomEvent('disableElement', {'detail': detail});
    window.dispatchEvent(event);
    $("#deselectWindow").modal("hide");
  });
};
