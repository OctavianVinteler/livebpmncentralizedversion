class SimulationEditor {

  constructor() {
    this.simulationStarted = false;
  }

  getElement(elementId, elementsList) {
    for(let element of elementsList) {
      if(elementId == element.id) {
        return element;
      }
    }
    return null;
  }

  processTaskSchema(inputSchema) {
    var processedSchema = '{\"type\": \"object\",\"properties\": {';

    for (var key in inputSchema) {
      if (inputSchema.hasOwnProperty(key)) {
        var val = inputSchema[key];
        processedSchema += '\"' + key + '\":{';

        switch (val) {
          case 'string':
            processedSchema += '\"type\": \"string\",\"required\": true},';
            break;
          case 'date':
            processedSchema += '\"type\": \"string\",\"format\": \"date\"},'
            break;
          case 'boolean':
            processedSchema += '\"type\": \"boolean\", \"format\": \"checkbox\"},';
            break;
          case 'integer':
            processedSchema += '\"type\": \"integer\"},';
            break;
          case 'number':
            processedSchema += '\"type\": \"number\"},';
            break;
          case 'color':
            processedSchema += '\"type\": \"string\",\"format\": \"color\"},';
            break;
        }
      }
    }

    // Remove the last comma from the generated json
    if(processedSchema.slice(-1) == ",") {
      processedSchema = processedSchema.substring(0, processedSchema.length - 1);
    }

    processedSchema += '}}';

    return JSON.parse(processedSchema);
  }

  selectPropertiesFromSchema(inputSchema) {
    //var processdInputSchema = this.selectPropertiesSchema(inputSchema);
    var selectPropertiesSchema = '{\"type\": \"object\",\"properties\": {';

    for (var key in inputSchema) {
      if(inputSchema.hasOwnProperty(key)) {
        selectPropertiesSchema += '\"' + key + '\":{';
        selectPropertiesSchema += '\"type\": \"boolean\",\"format\": \"checkbox\"},'
      }
    }

    // Remove the last comma from the generated json
    if(selectPropertiesSchema.slice(-1) == ",") {
      selectPropertiesSchema = selectPropertiesSchema.substring(0, selectPropertiesSchema.length - 1);
    }

    selectPropertiesSchema += '}}';

    //window.setTaskEditor(selectPropertiesSchema);
    return selectPropertiesSchema;
  }

  activateTaskProperties(activationSchema, dataObject) {
    // var patches = [];
    for (var key in activationSchema) {
      if(activationSchema.hasOwnProperty(key)) {
        var val = activationSchema[key];
        if(val) {
          dataObject.getEditor('root.' + key).enable();
          // display the fields which are supposed to be editables
          dataObject.getEditor('root.' + key).header.style.display = 'block';
          dataObject.getEditor('root.' + key).input.style.display = 'block';
        }
        else {
          dataObject.getEditor('root.' + key).disable();
          // hide the fields which are supposed to be not editables
          dataObject.getEditor('root.' + key).header.style.display = 'none';
          dataObject.getEditor('root.' + key).input.style.display = 'none';
        }
      }
    }
  }

}

module.exports = {
  SimulationEditor: SimulationEditor
};
